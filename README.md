# JIRA DC Balancer

## haproxy

```
haproxy:
  restart: unless-stopped
  image: haproxy
  container_name: haproxy
  ports:
    - 80:80
  networks:
    - intranet
  volumes:
    - /var/docker/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg:ro
```

## httpd

```
httpd:
  restart: unless-stopped
  image: httpd
  container_name: httpd
  ports:
    - 80:80
  networks:
    - intranet
  volumes:
    - /var/docker/httpd.conf:/usr/local/apache2/conf/httpd.conf:ro

```

## nginx

Nginx does not provide sticky proxy sessions. So it does not really make
sense to use it for this type of proxy.
